1:

    python manage.py makemigrations <app_name>
    
2:

    python manage.py makemigrations
    
3:

    python manage.py migrate
    
4:
Create super user

    python manage.py createsuperuser
    
    
 Running the application:
 
     python manage.py runserver

Running the application with costum port:

    python manage.py runserver <port>
