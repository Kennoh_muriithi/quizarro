from celery import shared_task, task
from celery.utils.log import get_task_logger
from celery import Celery
from datetime import datetime
from django.contrib.auth.models import User, Group
from django.core.mail import get_connection
from django.core.mail import EmailMessage
from django.core.mail.backends.smtp import EmailBackend
from django.template.loader import render_to_string
from django.conf import settings
from .models import *


@task(bind=True, reject_on_worker_lost=True, autoretry_for=(Exception,), retry_backoff=5, retry_jitter=True, retry_kwargs={'max_retries': 5})
def send_email_scores(self,email_data):
    email = email_data.get('email')
    total_score = email_data.get('total_score')
    user_score = email_data.get('user_score')
    judgement = email_data.get('judgement')
    quiz_id = email_data.get('id')
    quiz = Quiz.objects.get(id=quiz_id)
    context = {'total_score': total_score, 'user_score': user_score,'judgement': judgement, 'quiz': quiz}
    try:       
        message = render_to_string('core/quiz_scores.html', context)
        mail_subject = 'Quiz Result scores'
        to_email = email
        email = EmailMessage(mail_subject, message, to=[to_email])
        email.content_subtype = "html"
        email.send()
    except:
        self.retry()



