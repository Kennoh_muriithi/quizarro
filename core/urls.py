from django.contrib import admin
from django.urls import path
from django.conf.urls import url, include
from rest_framework import routers
from . import views


urlpatterns = [
    url(r'^core/quiz/(?P<id>[\w-]+)/$',views.QuizEditView.as_view(), name='update_or_delete_quiz'),
    url(r'^core/quiz/',views.GetQuizView.as_view(), name='get_create_quiz'),
    url(r'^core/question/(?P<id>[\w-]+)/$',views.QuestionsEditView.as_view(), name='update_or_delete_question'),
    url(r'^core/responses/(?P<id>[\w-]+)/$',views.GetQuizResponsesView.as_view(), name='get_all_responses'),
    url(r'^core/get-score/(?P<id>[\w-]+)/$',views.CalculateFinalScoreView.as_view(), name='get_final_score'),
    url(r'^core/email-score/(?P<id>[\w-]+)/$',views.EmailScoreView.as_view(), name='email_score'),
    url(r'^core/anonymous-responses/(?P<id>[\w-]+)/(?P<ip_address>[a-zA-Z0-9.]+)/$',views.GetQuizAnonymousResponsesView.as_view(), name='anonymous-responses'),
    url(r'^core/anonymous-scores/(?P<id>[\w-]+)/(?P<ip_address>[a-zA-Z0-9.]+)/$',views.AnonymousFinalScoreView.as_view(), name='anonymous-scores'),
    url(r'^core/email-anonymous-responses/(?P<id>[\w-]+)/(?P<ip_address>[a-zA-Z0-9.]+)/$',views.EmailAnonymousUserScoreView.as_view(), name='email-anonymous-responses'),
]



