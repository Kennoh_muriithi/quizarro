from django.db import models
from django.contrib.auth.models import User
from django.conf import settings
from django.db import models
from django.db.models import Q
from django.dispatch import receiver
from django.db.models.signals import post_save
import os
from django.utils import timezone
import string
import random
import six


""""Generates the unique ids where needed"""""
def id_generator(size=8, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


class QuizAnswers(models.Model):
    score = models.BigIntegerField()
    answer_text = models.TextField(blank=True, null=True)
    created_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.answer_text

class QuizResults(models.Model):
    Title = models.CharField(max_length=255)
    description = models.TextField()
    image = models.ImageField(upload_to='core/results', null=True, blank=True)

    def __str__(self):
        return self.Title


class QuizQuestions(models.Model):
    score = models.BigIntegerField()
    question = models.CharField(max_length=255)
    answers = models.ManyToManyField(QuizAnswers, related_name='answers')
    thumbnail = models.ImageField(upload_to='core/questions', null=True, blank=True)
    description = models.TextField(blank=True, null=True)
    created_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.question

class Quiz(models.Model):
    created_by = models.ForeignKey(User, on_delete=models.PROTECT)
    name = models.CharField(max_length=255)
    is_published = models.BooleanField(default=True)
    description = models.TextField(blank=True, null=True)
    questions = models.ManyToManyField(QuizQuestions, related_name='questions')
    quiz_results = models.ManyToManyField(QuizResults, related_name='quiz_results')
    created_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name


class Results(models.Model):
    question = models.ForeignKey(QuizQuestions, on_delete=models.CASCADE)
    answer = models.ForeignKey(QuizAnswers, on_delete=models.CASCADE)
    score = models.BigIntegerField(null=True, blank=True)

    def __str__(self):
        return self.score


class Responses(models.Model):
    quiz = models.ForeignKey(Quiz, on_delete=models.CASCADE)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    ip_address = models.GenericIPAddressField(null=True, blank=True)
    user_score = models.BigIntegerField(null=True, blank=True)
    final_score = models.BigIntegerField(null=True, blank=True)
    thumbnail = models.ImageField(upload_to='core/responses', null=True, blank=True)
    results = models.ManyToManyField(Results, related_name='results')
    judgement = models.ForeignKey(QuizResults, on_delete=models.CASCADE, null=True, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.user_score + "/" + self.final_score


def _delete_file(path):
   """ Deletes the actual file from filesystem."""
   if os.path.isfile(path):
       os.remove(path)


@receiver(models.signals.post_delete, sender=QuizQuestions)
def delete_file(sender, instance, *args, **kwargs):
    """ Deletes thumbnail files on `post_delete` """
    if instance.thumbnail:
        _delete_file(instance.thumbnail.path)


@receiver(models.signals.post_delete, sender=Responses)
def delete_file(sender, instance, *args, **kwargs):
    """ Deletes thumbnail files on `post_delete` """
    if instance.thumbnail:
        _delete_file(instance.thumbnail.path)

