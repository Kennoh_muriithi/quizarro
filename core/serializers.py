from .models import *
from rest_framework import serializers
from django.utils import timezone
import random


class AnswersSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = QuizAnswers
        fields = ['id', 'score', 'answer_text']
        extra_kwargs = {'answer_text': {'required': True},
                        'characteristics': {'required': True}}


class QuestionsSerializer(serializers.ModelSerializer):
    answers = AnswersSerializer(many=True)

    class Meta:
        model = QuizQuestions
        fields = ['id', 'question', 'score', 'answers','thumbnail', 'description', 'created_date']
        extra_kwargs = {'question': {'required': True},'score': {'required': True},'description': {'required': True}, }
        read_only_fields = ['id', 'created_date']

    def validate(self, data):
        if not bool(data.get('answers')):
            raise serializers.ValidationError("Please provide answers!")
        if bool(data.get('answers')):
            answer_dict = data.get('answers')
            question_score = data.get('score')
            for item in answer_dict:
                if int(item['score']) > int(question_score):
                    raise serializers.ValidationError("Answer score,"+str(item['score'])+" should not be greater than question's score,"+str(question_score))
        return data


class QuizResultsSerializer(serializers.ModelSerializer):

    class Meta:
        model = QuizResults
        fields = ['id', 'Title', 'description', 'image']

class QuizSerializer(serializers.ModelSerializer):
    questions = QuestionsSerializer(many=True)
    quiz_results = QuizResultsSerializer(many=True)

    class Meta:
        model = Quiz
        fields = ['id', 'name', 'is_published', 'description',
                  'questions', 'quiz_results', 'created_date']
        extra_kwargs = {'name': {'required': True},'description': {'required': True}, }
        read_only_fields = ['id', 'created_date']

    def validate(self, data):
        if not bool(data.get('questions')):
            raise serializers.ValidationError("Please provide questions!")
        return data

    def create(self, validated_data):
        questions = validated_data.pop('questions')
        quiz_results = validated_data.pop('quiz_results')
        quiz = Quiz.objects.create(**validated_data)
        for result in quiz_results:
            characteristics = QuizResults.objects.create(**result)
            quiz.quiz_results.add(characteristics)
        for question in questions:
            answer_dict = question.pop('answers')
            quiz_questions = QuizQuestions.objects.create(**question)
            quiz.questions.add(quiz_questions)
            for answer in answer_dict:
                added_answer = QuizAnswers.objects.create(**answer)
                quiz_questions.answers.add(added_answer)  
        return quiz


class QuizEditSerializer(serializers.ModelSerializer):
    questions = QuestionsSerializer(many=True,required=False)
    quiz_results = QuizResultsSerializer(many=True, required=False)

    class Meta:
        model = Quiz
        fields = ['id', 'name', 'is_published', 'description',
                  'questions', 'quiz_results', 'created_date']
        read_only_fields = ['id', 'created_date']
        extra_kwargs = {'name': {'required': False},'is_published': {'required': False},'description': {'required': False}, }

    def update(self, instance, validated_data):
        if bool(validated_data.get('name')):
            instance.name = validated_data.get('name', instance.name)
        if bool(validated_data.get('is_published')):
            instance.is_published = validated_data.get('is_published', instance.is_published)
        if bool(validated_data.get('description')):
            instance.description = validated_data.get('description', instance.description)
        if bool(validated_data.get('quiz_results')):
            characteristics = instance.quiz_results.all()
            for character in characteristics:
                character.delete()
            instance.quiz_results.clear()
            quiz_results = validated_data.pop('quiz_results')
            for result in quiz_results:
                results = QuizResults.objects.create(**result)
            instance.quiz_results.add(results)
        if bool(validated_data.get('questions')):
            original_questions = instance.questions.all()
            for question in original_questions:
                answers = question.answers.all()
                for answer in answers:
                    answer.delete()
                question.answers.clear()
                question.delete()
            instance.questions.clear()
            questions = validated_data.pop('questions')
            for question in questions:
                answer_dict = question.pop('answers')
                quiz_questions = QuizQuestions.objects.create(**question)
                instance.questions.add(quiz_questions)
                for answer in answer_dict:
                    added_answer = QuizAnswers.objects.create(**answer)
                    quiz_questions.answers.add(added_answer)
        instance.save()
        return instance


class QuestionsEditSerializer(serializers.ModelSerializer):
    answers = AnswersSerializer(many=True,required=False)

    class Meta:
        model = QuizQuestions
        fields = ['id', 'question', 'score', 'answers','thumbnail', 'description', 'created_date']
        extra_kwargs = {'question': {'required': False}, 'score': {'required': False}, 'description': {'required': False}, }
        read_only_fields = ['id', 'created_date']

    def validate(self, data):
        if not bool(data.get('answers')):
            raise serializers.ValidationError("Please provide answers!")
        if bool(data.get('answers')) and bool(data.get('score')):
            answer_dict = data.get('answers')
            question_score = data.get('score')
            for item in answer_dict:
                if int(item['score']) > int(question_score):
                    raise serializers.ValidationError("Answer score,"+str(item['score'])+" should not be greater than question's score,"+str(question_score))
        return data


    def update(self, instance, validated_data):
        if bool(validated_data.get('score')):
            instance.score = validated_data.get('score', instance.score)
        if bool(validated_data.get('question')):
            instance.question = validated_data.get('question', instance.question)
        if bool(validated_data.get('thumbnail')):
            instance.thumbnail = validated_data.get('thumbnail', instance.thumbnail)
        if bool(validated_data.get('description')):
            instance.description = validated_data.get('description', instance.description)
        if bool(validated_data.get('answers')):
            answer_dict = validated_data.pop('answers')
            if not bool(validated_data.get('score')):
                for score in answer_dict:
                    if int(score.get('score')) > int(instance.score):
                        raise serializers.ValidationError("Answer score, "+str(score.get('score'))+" should not be greater than question score, "+str(instance.score))
            answer_list = instance.answers.all()
            for answer in answer_list:
                answer.delete()
                instance.answers.clear()
            for answer in answer_dict:
                added_answer = QuizAnswers.objects.create(**answer)
                instance.answers.add(added_answer)
        instance.save()
        return instance


class ResultSerializer(serializers.ModelSerializer):
    question = serializers.SlugRelatedField(queryset=QuizQuestions.objects.all(), required=True, slug_field='id')
    answer = serializers.SlugRelatedField(queryset=QuizAnswers.objects.all(), required=True, slug_field='id')

    class Meta:
        model = Results
        fields = ['id', 'question', 'answer', 'score']
        read_only_fields = ['id', 'score']


class UserResponseSerializer(serializers.ModelSerializer):
    results = ResultSerializer(many=True)
    # judgement = QuizResultsSerializer(read_only=True)


    class Meta:
        model = Responses
        fields = ['id', 'user_score', 'final_score',
                  'thumbnail', 'created_date', 'results']
        read_only_fields = ['id', 'created_date', 'user_score', 'final_score']

    def create(self, validated_data):
        results = validated_data.pop('results')
        user_score = 0
        total_score = 0
        for score in results:
            answer_score = score.get('answer').score
            question_score = score.get('question').score
            user_score += int(answer_score)
            total_score += int(question_score)
        response_obj = Responses.objects.create(user_score=user_score, final_score=total_score, **validated_data)
        for result in results:
            answer_score = result.get('answer').score
            chosen_result = Results.objects.create(score=answer_score, **result)
            response_obj.results.add(chosen_result)
        return response_obj


class UserGetResponseSerializer(serializers.ModelSerializer):
    results = ResultSerializer(many=True)
    judgement = QuizResultsSerializer(read_only=True)

    class Meta:
        model = Responses
        fields = ['id', 'user_score', 'final_score',
                  'thumbnail', 'created_date', 'judgement', 'results']
        read_only_fields = ['id', 'created_date',
                            'judgement', 'user_score', 'final_score']




class UserFetchResultSerializer(serializers.ModelSerializer):
    judgement = QuizResultsSerializer(read_only=True)
    quiz = serializers.SlugRelatedField(read_only=True, slug_field='name')

    class Meta:
        model = Responses
        fields = ['id', 'quiz', 'user_score', 'final_score', 'judgement']
        


class EmailSerializer(serializers.Serializer):
    email = serializers.EmailField()

    class Meta:
        fields = '__all__'
