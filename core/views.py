from django.shortcuts import render
from .serializers import *
from .models import *
from authentication.tasks import user_logging
from django.http.response import JsonResponse
from rest_framework.parsers import JSONParser
from rest_framework import status
from django.contrib.auth.models import User, Group
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from django.views.decorators.csrf import csrf_exempt
from rest_framework.authtoken.models import Token
from django.db.models import Q
from rest_framework import mixins, generics
from django.http import Http404
from rest_framework import permissions
from rest_framework.parsers import JSONParser, FormParser, MultiPartParser
from django.utils.decorators import method_decorator
from drf_yasg.utils import swagger_auto_schema
from .tasks import send_email_scores

class GetQuizView(generics.ListAPIView, generics.CreateAPIView):
    authentication_classes = (JSONWebTokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = QuizSerializer
    parser_classes = [JSONParser, FormParser, MultiPartParser]

    @method_decorator(name='get', decorator=swagger_auto_schema(
        operation_id='Get all instances of quizzes available',
        operation_description="""
        An Api View which provides a GET method to get all instances of quizzes, their questions and their answers available.
        It accepts a jwt token for authentication.
        """))
    def get(self, request):
        queryset = Quiz.objects.all().filter(created_by=request.user).order_by('-created_date')
        serializer = QuizSerializer(queryset, many=True)
        return JsonResponse(serializer.data, safe=False)

    @method_decorator(name='post', decorator=swagger_auto_schema(
        operation_id='Create instances of quizzes',
        operation_description="""
        An Api View which provides a POST method to create instance of a quiz, it's questions and the corresponding answers.
        It accepts a jwt token for authentication.
        """))
    def post(self, request, format=None):
        serializer = QuizSerializer(data=request.data)
        if serializer.is_valid():
            quiz = serializer.save(created_by=self.request.user)
            description = 'Created a quiz instance'
            table_name = quiz._meta.db_table
            unique_id = quiz.id
            data = {"description": description, 'table_name': table_name,'unique_id': unique_id, "id": self.request.user.id}
            user_logging.delay(data)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class QuizEditView(generics.RetrieveUpdateDestroyAPIView):
    authentication_classes = (JSONWebTokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = QuizEditSerializer
    parser_classes = [JSONParser, FormParser, MultiPartParser]


    def get_object(self, id):
        """
        An Api View which provides a GET method to get an instance of quiz , it's questions and the corresponding answers.
        It accepts a jwt token.
        """
        try:
            return Quiz.objects.get(id=id)
        except Quiz.DoesNotExist:
            raise Http404

    @method_decorator(name='get', decorator=swagger_auto_schema(
        operation_id='Get an instance of quiz by id',
        operation_description="""
        An Api View which provides a GET method to get an instance of a quiz , it's questions and the corresponding answers.
        It accepts a jwt token.
        """))
    def get(self, request, id, format=None):
        snippet = self.get_object(id)
        serializer = QuizEditSerializer(snippet)
        return Response(serializer.data)

    @method_decorator(name='put', decorator=swagger_auto_schema(
        operation_id='Edit an instance of quiz',
        operation_description="""
        An Api View which provides a PUT method to edit an instance of quiz , it's questions and the corresponding answers.
        It accepts a jwt token.
        """))
    def put(self, request, id, format=None):
        snippet = self.get_object(id)
        serializer = QuizEditSerializer(snippet, data=request.data)
        if serializer.is_valid():
            quiz = serializer.save()
            description = 'Edited a quiz instance'
            table_name = quiz._meta.db_table
            unique_id = quiz.id
            data = {"description": description, 'table_name': table_name,'unique_id': unique_id, "id": self.request.user.id}
            user_logging.delay(data)
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @method_decorator(name='delete', decorator=swagger_auto_schema(
        operation_id='Delete an instance of Quiz',
        operation_description="""
        An Api View which provides a DELETE method to purge an instance of quiz , it's questions and the corresponding answers.
        It accepts a jwt token.
        """))
    def delete(self, request, id, format=None):
        quiz = self.get_object(id)
        original_questions = quiz.questions.all()
        for question in original_questions:
            answers = question.answers.all()
            for answer in answers:
                answer.delete()
            question.answers.clear()
            question.delete()
        quiz.questions.clear()
        characteristics = quiz.quiz_results.all()
        for character in characteristics:
            character.delete()
        quiz.quiz_results.clear()
        description = 'Deleted a quiz instance'
        table_name = quiz._meta.db_table
        unique_id = quiz.id
        data = {"description": description, 'table_name': table_name,'unique_id': unique_id, "id": self.request.user.id}
        user_logging.delay(data)
        quiz.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class QuestionsEditView(generics.RetrieveUpdateDestroyAPIView):
    authentication_classes = (JSONWebTokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = QuestionsEditSerializer
    parser_classes = [JSONParser, FormParser, MultiPartParser]

    def get_object(self, id):
        """
        An Api View which provides a GET method to get an instance of a question and answers in a quiz.
        It accepts a jwt token.
        """
        try:
            return QuizQuestions.objects.get(id=id)
        except QuizQuestions.DoesNotExist:
            raise Http404

    @method_decorator(name='get', decorator=swagger_auto_schema(
        operation_id='Get an instance of question in a quiz',
        operation_description="""
        An Api View which provides a GET method to get an instance of a question and its answers.
        It accepts a jwt token.
        """))
    def get(self, request, id, format=None):
        snippet = self.get_object(id)
        serializer = QuestionsEditSerializer(snippet)
        return Response(serializer.data)

    @method_decorator(name='put', decorator=swagger_auto_schema(
        operation_id='Edit an instance of quiz question',
        operation_description="""
        An Api View which provides a PUT method to edit an instance of a question and it's answers.
        It accepts a jwt token.
        """))
    def put(self, request, id, format=None):
        snippet = self.get_object(id)
        serializer = QuestionsEditSerializer(snippet, data=request.data)
        if serializer.is_valid():
            quiz_question = serializer.save()
            description = 'Edited a quiz questions instance'
            table_name = quiz_question._meta.db_table
            unique_id = quiz_question.id
            data = {"description": description, 'table_name': table_name,'unique_id': unique_id, "id": self.request.user.id}
            user_logging.delay(data)
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @method_decorator(name='delete', decorator=swagger_auto_schema(
        operation_id='Delete an instance of question',
        operation_description="""
        An Api View which provides a DELETE method to purge an instance of question and it's answers.
        It accepts a jwt token.
        """))
    def delete(self, request, id, format=None):
        quiz_question = self.get_object(id)
        answers = quiz_question.answers.all()
        for answer in answers:
            answer.delete()
        quiz_question.answers.clear()
        description = 'Deleted a quiz questions instance'
        table_name = quiz_question._meta.db_table
        unique_id = quiz_question.id
        data = {"description": description, 'table_name': table_name,'unique_id': unique_id, "id": self.request.user.id}
        user_logging.delay(data)
        quiz_question.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
    

class GetQuizResponsesView(generics.ListAPIView, generics.CreateAPIView):
    authentication_classes = (JSONWebTokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = UserResponseSerializer
    parser_classes = [JSONParser, FormParser, MultiPartParser]

    @method_decorator(name='get', decorator=swagger_auto_schema(
        operation_id='Retrieve all instances of responses for a particular quiz',
        operation_description="""
        An Api View which provides a GET method to retrieve all the questions and responses answered by a user in a particular quiz.
        It accepts a jwt token.
        """))
    def get(self, request, id, format=None):
        try:
            quiz = Quiz.objects.get(id=id)
            responses = Responses.objects.all().filter(created_by=self.request.user, quiz=quiz)
            serializer = UserGetResponseSerializer(responses, many=True)
            return JsonResponse(serializer.data, safe=False)
        except Quiz.DoesNotExist:
            raise Http404

    @method_decorator(name='post', decorator=swagger_auto_schema(
    operation_id='Create instances of quiz responses',
    operation_description="""
    An Api View which provides a POST method to create instance of a responses.
    It accepts a jwt token for authentication.
    """))
    def post(self, request, id, format=None):
        try:
            quiz = Quiz.objects.get(id=id)
        except Quiz.DoesNotExist:
            raise Http404
        serializer = UserResponseSerializer(data=request.data)
        if serializer.is_valid():
            if Responses.objects.all().filter(created_by=self.request.user, quiz=quiz).exists():
                return Response({'error': "Sorry, You are only allowed one attempt per quiz."}, status=status.HTTP_403_FORBIDDEN)
            question_number = 0
            questions = quiz.questions.all()
            for item in request.data['results']:
                question_number += 1
                if not questions.filter(id=int(item['question'])).exists():
                    return Response({'error': "This question, "+str(item['question'])+" does not belong to the quiz,"+str(quiz.id)}, status=status.HTTP_403_FORBIDDEN)
                question = QuizQuestions.objects.get(id=int(item.get('question')))
                answers = question.answers.all()
                if not answers.filter(id=int(item['answer'])).exists():
                    return Response({'error': "This answer, "+str(item['answer'])+" does not belong to the question, "+str(question.id)}, status=status.HTTP_403_FORBIDDEN)
            if question_number > questions.count():
                return Response({'error': "You have provided more questions,"+str(question_number)+" than the quiz has, "+str(questions.count())+" ,check for duplicates."}, status=status.HTTP_403_FORBIDDEN)
            characteristics = quiz.quiz_results.all()
            character = random.choice(characteristics)
            response = serializer.save(created_by=self.request.user, judgement=character, quiz=quiz)
            description = 'Created a response for a quiz'
            table_name = response._meta.db_table
            unique_id = response.id
            data = {"description": description, 'table_name': table_name,'unique_id': unique_id, "id": self.request.user.id}
            user_logging.delay(data)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        


class CalculateFinalScoreView(generics.RetrieveAPIView):
    authentication_classes = (JSONWebTokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = UserFetchResultSerializer
    parser_classes = [JSONParser, FormParser, MultiPartParser]

    @method_decorator(name='get', decorator=swagger_auto_schema(
        operation_id='Calculate the final score for a particular quiz',
        operation_description="""
        An Api View which provides a GET method to get score of a particular quiz.
        It accepts a jwt token.
        """))
    def get(self, request, id, format=None):
        try:
            quiz = Quiz.objects.get(id=id)
            responses = Responses.objects.all().filter(created_by=self.request.user, quiz=quiz)
            if not responses.exists():
                return Response({'error': 'There exists no responses for this quiz and user'}, status=status.HTTP_403_FORBIDDEN)
            serializer = UserFetchResultSerializer(responses, many=True)
            return JsonResponse(serializer.data, safe=False)
        except Quiz.DoesNotExist:
            raise Http404



class EmailScoreView(generics.CreateAPIView):
    authentication_classes = (JSONWebTokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = EmailSerializer
    parser_classes = [JSONParser, FormParser, MultiPartParser]

    @method_decorator(name='post', decorator=swagger_auto_schema(
        operation_id='Email a user quiz scores',
        operation_description="""
        An Api View which provides a POST method to email a user their quiz scores.
        It accepts a jwt token.
        """))
    def post(self, request, id,format=None):
        try:
            quiz = Quiz.objects.get(id=id)
            responses = Responses.objects.all().filter(created_by=self.request.user, quiz=quiz)
            if not responses.exists():
                return Response({'error': 'There exists no responses for this quiz and user'}, status=status.HTTP_403_FORBIDDEN)
            serializer = EmailSerializer(data=request.data)
            if serializer.is_valid():
                email = request.data['email']
                description = 'Email User quiz scores'
                table_name = 'User quiz responses'
                unique_id = email
                data = {"description": description, 'table_name': table_name,'unique_id': unique_id, "id": self.request.user.id}
                user_logging.delay(data)
                user_score = int(responses.first().user_score)
                total_score = int(responses.first().final_score)
                judgement = responses.first().judgement.description
                email_data = {"user_score": user_score, "total_score": total_score,'judgement': judgement, "email": email, 'id': id}
                send_email_scores.delay(email_data)
                return Response(serializer.data, status=status.HTTP_200_OK)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        except Quiz.DoesNotExist:
            raise Http404


class GetQuizAnonymousResponsesView(generics.ListAPIView, generics.CreateAPIView):
    authentication_classes = ()
    permission_classes = ()
    serializer_class = UserResponseSerializer
    parser_classes = [JSONParser, FormParser, MultiPartParser]

    @method_decorator(name='get', decorator=swagger_auto_schema(
    operation_id='Retrieve all instances of responses for an anonymous',
    operation_description="""
    An Api View which provides a GET method to retrieve all the questions and responses answered by an anonymous user in a particular quiz.
    """))
    def get(self, request, id, ip_address, format=None):
        try:
            quiz = Quiz.objects.get(id=id)
            responses = Responses.objects.all().filter(ip_address=ip_address, quiz=quiz)
            serializer = UserGetResponseSerializer(responses, many=True)
            return JsonResponse(serializer.data, safe=False)
        except Quiz.DoesNotExist:
            raise Http404

    @method_decorator(name='post', decorator=swagger_auto_schema(
    operation_id='Create instances of quiz responses for anonymous user',
    operation_description="""
    An Api View which provides a POST method to create instance of a responses for an anonymous user.
    """))
    def post(self, request, id, ip_address, format=None):
        try:
            quiz = Quiz.objects.get(id=id)
        except Quiz.DoesNotExist:
            raise Http404
        serializer = UserResponseSerializer(data=request.data)
        if serializer.is_valid():
            if Responses.objects.all().filter(ip_address=ip_address, quiz=quiz).exists():
                return Response({'error': "Sorry, You are only allowed one attempt per quiz."}, status=status.HTTP_403_FORBIDDEN)
            question_number = 0
            questions = quiz.questions.all()
            for item in request.data['results']:
                question_number += 1
                if not questions.filter(id=int(item['question'])).exists():
                    return Response({'error': "This question, "+str(item['question'])+" does not belong to the quiz,"+str(quiz.id)}, status=status.HTTP_403_FORBIDDEN)
                question = QuizQuestions.objects.get(
                    id=int(item.get('question')))
                answers = question.answers.all()
                if not answers.filter(id=int(item['answer'])).exists():
                    return Response({'error': "This answer, "+str(item['answer'])+" does not belong to the question, "+str(question.id)}, status=status.HTTP_403_FORBIDDEN)
            if question_number > questions.count():
                return Response({'error': "You have provided more questions,"+str(question_number)+" than the quiz has, "+str(questions.count())+" ,check for duplicates."}, status=status.HTTP_403_FORBIDDEN)
            characteristics = quiz.quiz_results.all()
            character = random.choice(characteristics)
            response = serializer.save(ip_address=ip_address, judgement=character, quiz=quiz)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class AnonymousFinalScoreView(generics.RetrieveAPIView):
    authentication_classes = ()
    permission_classes = ()
    serializer_class = UserFetchResultSerializer
    parser_classes = [JSONParser, FormParser, MultiPartParser]

    @method_decorator(name='get', decorator=swagger_auto_schema(
        operation_id='Calculate the final score for a particular quiz for an anonymous user',
        operation_description="""
        An Api View which provides a GET method to get score of a particular quiz for an anonymous user.
        It accepts a jwt token.
        """))
    def get(self, request, id, ip_address, format=None):
        try:
            quiz = Quiz.objects.get(id=id)
            responses = Responses.objects.all().filter(ip_address=ip_address, quiz=quiz)
            if not responses.exists():
                return Response({'error': 'There exists no responses for this quiz and user'}, status=status.HTTP_403_FORBIDDEN)
            serializer = UserFetchResultSerializer(responses, many=True)
            return JsonResponse(serializer.data, safe=False)
        except Quiz.DoesNotExist:
            raise Http404


class EmailAnonymousUserScoreView(generics.CreateAPIView):
    authentication_classes = ()
    permission_classes = ()
    serializer_class = EmailSerializer
    parser_classes = [JSONParser, FormParser, MultiPartParser]

    @method_decorator(name='post', decorator=swagger_auto_schema(
        operation_id='Email an anonymous user quiz scores',
        operation_description="""
        An Api View which provides a POST method to email an anonymous user their quiz scores.
        It accepts a jwt token.
        """))
    def post(self, request, id, ip_address, format=None):
        try:
            quiz = Quiz.objects.get(id=id)
            responses = Responses.objects.all().filter(ip_address=ip_address, quiz=quiz)
            if not responses.exists():
                return Response({'error': 'There exists no responses for this quiz'}, status=status.HTTP_403_FORBIDDEN)
            serializer = EmailSerializer(data=request.data)
            if serializer.is_valid():
                email = request.data['email']
                user_score = int(responses.first().user_score)
                total_score = int(responses.first().final_score)
                judgement = responses.first().judgement.description
                email_data = {"user_score": user_score, "total_score": total_score,
                              'judgement': judgement, "email": email, 'id': id}
                send_email_scores.delay(email_data)
                return Response(serializer.data, status=status.HTTP_200_OK)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        except Quiz.DoesNotExist:
            raise Http404
