# Generated by Django 3.1.2 on 2021-02-13 07:55

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0022_responses_judgement'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='quizanswers',
            name='characteristics',
        ),
        migrations.CreateModel(
            name='QuizCharacteristics',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('characteristics', models.TextField()),
                ('answer', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='answer', to='core.quizanswers')),
            ],
        ),
    ]
