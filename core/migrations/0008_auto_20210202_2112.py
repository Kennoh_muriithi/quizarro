# Generated by Django 3.1.2 on 2021-02-02 18:12

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0007_auto_20210202_2112'),
    ]

    operations = [
        migrations.AlterField(
            model_name='responses',
            name='quiz',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='core.quiz'),
        ),
    ]
