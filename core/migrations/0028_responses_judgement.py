# Generated by Django 3.1.2 on 2021-02-15 20:30

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0027_remove_responses_judgement'),
    ]

    operations = [
        migrations.AddField(
            model_name='responses',
            name='judgement',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='core.quizresults'),
        ),
    ]
