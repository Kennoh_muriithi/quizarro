# Generated by Django 3.1.2 on 2021-02-03 19:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0010_auto_20210203_2252'),
    ]

    operations = [
        migrations.AlterField(
            model_name='quizquestions',
            name='score',
            field=models.IntegerField(),
        ),
    ]
