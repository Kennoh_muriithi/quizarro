from django.contrib.auth.hashers import make_password
from django.contrib.auth.base_user import BaseUserManager
import requests
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate
from django.http.response import JsonResponse
from rest_framework.parsers import JSONParser
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes, authentication_classes
from django.contrib.auth.models import User, Group, Permission
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework.authentication import SessionAuthentication, BasicAuthentication, TokenAuthentication
from django.views.decorators.csrf import csrf_exempt
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.response import Response
from rest_framework import mixins, generics
from django.db.models import Q
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.contrib.auth.models import update_last_login
from rest_framework import serializers
from .serializers import *
from rest_framework_jwt.settings import api_settings
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework.parsers import JSONParser, FormParser, MultiPartParser
from django.http import Http404
from django.utils.decorators import method_decorator
from .serializers import *
from drf_yasg.utils import swagger_auto_schema
from rest_framework.utils import json
from datetime import datetime
import calendar
JWT_PAYLOAD_HANDLER = api_settings.JWT_PAYLOAD_HANDLER
JWT_ENCODE_HANDLER = api_settings.JWT_ENCODE_HANDLER


class LoginView(ObtainAuthToken):
    serializer_class = GoogleTokenSerializer
    parser_classes = [JSONParser, FormParser, MultiPartParser]

    @method_decorator(name='post', decorator=swagger_auto_schema(
    operation_id='Get jwt token for authentication and sign in access',
    operation_description="""
    An Api View which provides a method to request a jwt token authentication using goole authentication.
    It accepts a google token and returns a jwt token.
    """))
    def post(self, request, *args, **kwargs):
        serializer = GoogleTokenSerializer(data=request.data)
        if serializer.is_valid():
            payload = {'access_token': request.data.get("token")} 
            r = requests.get('https://www.googleapis.com/oauth2/v2/userinfo', params=payload)
            data = json.loads(r.text)
            if 'error' in data:
                content = {'message': 'Invalid google token, this google token is already expired.'}
                return Response(content)
            try:
                user = User.objects.get(email=data['email'])
            except User.DoesNotExist:
                user = User()
                user.first_name = data['given_name']
                user.last_name = data['family_name']
                user.username = data['email']
                user.password = make_password(BaseUserManager().make_random_password())
                user.email = data['email']
                user.is_active = True
                user.save()
            try:
                if user.is_active == False:
                    return Response({'error': 'This user account has been suspended, contact the admin for support'}, status=status.HTTP_403_FORBIDDEN)
                response = {}
                response['username'] = user.username
                payload = JWT_PAYLOAD_HANDLER(user)
                if api_settings.JWT_ALLOW_REFRESH:
                    payload['orig_iat'] = calendar.timegm(datetime.utcnow().utctimetuple())
                jwt_token = JWT_ENCODE_HANDLER(payload)
                update_last_login(None, user)
                response['jwt_token'] = jwt_token
                response['profile_picture'] = data['picture']
                return Response(response)
            except:
                return Response({'error': 'An error occcured authenticating your credentials, try again.'}, status=status.HTTP_404_NOT_FOUND)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
