from django.contrib import admin
from django.urls import path
from django.conf.urls import url, include
from . import views
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView

urlpatterns = [
    url(r'^google/', include('rest_framework_social_oauth2.urls')),
    url(r'^login/', views.LoginView.as_view(), name='login'),
]

