from celery import shared_task, task
from celery.schedules import crontab
from celery.decorators import periodic_task
from celery.utils.log import get_task_logger
from celery import Celery
from datetime import datetime
from .models import *
from django.contrib.auth.models import User, Group



@task(reject_on_worker_lost=True)
def user_logging(data):
    description = data.get('description')
    id = data.get('id')
    unique_id = data.get('unique_id')
    table_name = data.get('table_name')
    try:
        user = User.objects.get(id=id)
        User_logs.objects.create(user=user,unique_id=unique_id,table_name=table_name,description=description)
    except:
        response = "Problem creating user log"
        return response
