from django.contrib.auth.models import User, Group
from rest_framework import serializers



class GoogleTokenSerializer(serializers.Serializer):
    token = serializers.CharField(max_length=1000)

    class Meta:
        fields = ['token']
